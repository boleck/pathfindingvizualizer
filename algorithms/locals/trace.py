from algorithms.locals.straight_path import StraightPath
from selection import FieldType
from utils.directions import Direction


class Trace(StraightPath):

    def __init__(self):
        super().__init__()
        self.is_tracking = False
        self.followed_cell = (0, 0)

    def next_step(self):
        straight_x, straight_y = self.straight_move()
        self.decide_tracking(straight_x, straight_y)

    def move_to(self, x, y):
        if self.is_tracking and self.should_stop_tracking(x, y):
            self.is_tracking = False
        super().move_to(x, y)

    def begin_trace(self, x, y):
        self.followed_cell = (x + self.walker_x, y + self.walker_y)
        self.trace()

    def trace(self):
        self.is_tracking = True
        direction = Direction.get_value_by_position(self.followed_cell[0] - self.walker_x,
                                                    self.followed_cell[1] - self.walker_y)
        found_wall = False
        for i in range(8):
            cell = (i + direction.value) % 8
            cell_direction = Direction(cell).points()
            if found_wall and self.is_clear_to_move(cell_direction[0] + self.walker_x,
                                                    cell_direction[1] + self.walker_y):
                prev_cell = self.clockwise_prev_direction(cell).points()
                self.followed_cell = (prev_cell[0] + self.walker_x, prev_cell[1] + self.walker_y)
                self.move_to(cell_direction[0], cell_direction[1])
                break
            else:
                found_wall = self.matrix[self.walker_y + cell_direction[1]][
                                 self.walker_x + cell_direction[0]].select_type == FieldType.WALL

    def decide_tracking(self, x, y):
        if self.is_tracking:
            self.trace()
        elif self.is_clear_to_move(x + self.walker_x, y + self.walker_y):
            self.move_to(x, y)
        else:
            self.begin_trace(x, y)

    def should_stop_tracking(self, x, y):
        pass

    def clockwise_prev_direction(self, value):
        return Direction((value - 1) % 8)

    def clockwise_next_direction(self, value):
        return Direction((value + 1) % 8)
