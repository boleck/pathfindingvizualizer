from random import randrange

from algorithms.locals.straight_path import StraightPath
from utils.directions import Direction


class RandomBounce(StraightPath):

    def __init__(self):
        super().__init__()

    def next_step(self):
        x, y = self.straight_move()

        while not self.is_clear_to_move(x + self.walker_x, y + self.walker_y):
            x, y = Direction(randrange(8)).points()

        self.move_to(x, y)
