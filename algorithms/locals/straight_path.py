from algorithms.algorithm import Algorithm
import math
from selection import FieldType

from utils.directions import Direction


class StraightPath(Algorithm):

    def __init__(self):
        super().__init__()

    def next_step(self):
        x, y = self.straight_move()
        self.move_to(x, y)

    def straight_move(self):
        a = (self.walker_x, self.walker_y)
        b = (self.end_x, self.end_y)

        r = math.atan2(b[1] - a[1], b[0] - a[0])
        step = math.pi / 8
        if -step <= r < step:
            return Direction.E.points()
        elif step <= r < (math.pi / 2) - step:
            return Direction.SE.points()
        elif (math.pi / 2) - step <= r < math.pi / 2 + step:
            return Direction.S.points()
        elif math.pi / 2 + step <= r < math.pi - step:
            return Direction.SW.points()
        elif math.pi - step <= r or r < -(math.pi - step):
            return Direction.W.points()
        elif -(math.pi - step) <= r < -(math.pi / 2 + step):
            return Direction.NW.points()
        elif -(math.pi / 2 + step) <= r < -(math.pi / 2 - step):
            return Direction.N.points()
        elif -(math.pi / 2 - step) <= r < - step:
            return Direction.NE.points()

    def is_clear_to_move(self, x, y):
        if x < 0 or y < 0 or y >= len(self.matrix) or x >= len(self.matrix[y]):
            return False
        return self.matrix[y][x].select_type != FieldType.WALL

    def move_to(self, x, y):
        self.lines.append((self.walker_x, self.walker_y, self.walker_x + x, self.walker_y + y))
        self.walker_x += x
        self.walker_y += y
        # self.matrix[self.walker_y][self.walker_x].select_type = FieldType.PATH
        self.changes.append((self.walker_x, self.walker_y))

