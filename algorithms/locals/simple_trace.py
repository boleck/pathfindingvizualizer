
from algorithms.locals.trace import Trace
from utils.directions import Direction


class SimpleTrace(Trace):

    def __init__(self):
        super().__init__()
        self.goal_direction = None

    def next_step(self):
        straight_x, straight_y = self.straight_move()
        self.goal_direction = Direction.get_value_by_position(straight_x, straight_y)
        self.decide_tracking(straight_x, straight_y)

    def is_heading_to_direction(self, heading_now):
        return heading_now == self.goal_direction \
               or heading_now == self.clockwise_prev_direction(self.goal_direction.value) \
               or heading_now == self.clockwise_next_direction(self.goal_direction.value)

    def should_stop_tracking(self, x, y):
        heading_now = Direction.get_value_by_position(x, y)
        return self.is_heading_to_direction(heading_now)
