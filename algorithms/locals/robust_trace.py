import math

from algorithms.locals.trace import Trace
from selection import FieldType
from utils.constants import Constant

cell_size = Constant.CELL_SIZE
offset = Constant.OFFSET


class RobustTrace(Trace):

    def __init__(self):
        super().__init__()
        self.change = False
        self.trace_begin_x = self.start_x
        self.trace_begin_y = self.start_y
        self.a = 0
        self.b = 0

    def calculate_line_of_sight(self, x, y):
        x_a = ((x * cell_size) + offset) + cell_size / 2
        y_a = ((y * cell_size) + offset) + cell_size / 2
        x_b = (((self.end_x * cell_size) + offset) + cell_size / 2) + 0.00001
        y_b = (((self.end_y * cell_size) + offset) + cell_size / 2)
        self.a = ((y_a - y_b) / (x_a - x_b)) + 0.00001
        self.b = (y_a - (((y_a - y_b) / (x_a - x_b)) * x_a))

    def contains_line_of_sight(self, cell_x, cell_y):
        x = (cell_x * cell_size) + cell_size/2 + offset
        y = (cell_y * cell_size) + cell_size/2 + offset
        (n_x, n_y) = self.get_closest_point_to_line_of_sight(x, y)
        return x + cell_size/2 + offset >= n_x >= x - (cell_size/2 + offset) and y + cell_size/2 + offset >= n_y >= y - (cell_size/2 + offset)

    def get_closest_point_to_line_of_sight(self, x, y):
        a2 = (-(1 / self.a))
        b2 = y - a2 * x

        x1 = (b2 - self.b) / (self.a - a2)
        y1 = self.a * x1 + self.b
        return x1, y1

    def is_above_origin(self, x, y):
        a2 = (-(1 / self.a))
        b2 = ((self.trace_begin_y * cell_size) + cell_size/2 + offset) - a2 * ((self.trace_begin_x * cell_size) + cell_size/2 + offset)

        return ((y * cell_size) + offset) > a2 * ((x * cell_size) + offset) + b2

    def begin_trace(self, x, y):
        self.trace_begin_x = self.walker_x
        self.trace_begin_y = self.walker_y
        # self.lines.append((self.walker_x, self.walker_y, self.end_x, self.end_y))
        self.calculate_line_of_sight(self.trace_begin_x, self.trace_begin_y)
        super().begin_trace(x, y)

    def should_stop_tracking(self, x, y):
        return self.contains_line_of_sight(self.walker_x, self.walker_y) and not self.is_origin_in_between() and\
               (self.trace_begin_x != self.walker_x or self.trace_begin_y != self.walker_y) and self.can_move_to()

    def is_origin_in_between(self):
        return self.is_above_origin(self.walker_x, self.walker_y) != self.is_above_origin(self.end_x, self.end_y)

    def can_move_to(self):
        x, y = self.straight_move()
        return self.get_field(self.walker_x + x, self.walker_y + y) != FieldType.WALL
