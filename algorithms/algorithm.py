from selection import FieldType


class Algorithm:
    is_running = False

    def __init__(self):
        self.matrix = [[]]
        self.start_x = 0
        self.start_y = 0
        self.end_x = 0
        self.end_y = 0
        self.walker_x = 0
        self.walker_y = 0
        self.changes = []
        self.should_clean_lines = False
        self.lines = []

    def setup_matrix(self, matrix):
        self.matrix = matrix

    def update_matrix(self):
        return self.matrix

    def next_step(self):
        pass

    def set_options(self, array):
        for key in array:
            setattr(self, key, array[key])

    def get_lines(self):
        result, self.lines = self.lines, []
        return result

    def get_field(self, x, y):
        if x < 0 or y < 0 or y >= len(self.matrix) or x >= len(self.matrix[y]):
            return FieldType.WALL
        return self.matrix[y][x].select_type

    def set_status(self, x, y, select_type):
        self.changes.append((x, y))
        self.matrix[y][x].select_type = select_type

    def setup_starts_points(self, start_x, start_y):
        self.start_x = start_x
        self.start_y = start_y
        self.walker_x = start_x
        self.walker_y = start_y

    def setup_ends_points(self, end_x, end_y):
        self.end_x = end_x
        self.end_y = end_y

    def clean_lines(self):
        return self.should_clean_lines

    def get_changes(self):
        result, self.changes = self.changes, []
        return result

    def finish(self):
        self.walker_x = self.end_x
        self.walker_y = self.end_y
