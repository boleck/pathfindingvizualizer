from algorithms.advanced.dfs import Dfs
from algorithms.algorithm import Algorithm
from selection import FieldType


class Iddfs(Algorithm):
    def __init__(self):
        super().__init__()
        self.alg = Dfs()
        self.checked_depth = 29
        self.max_depth = 0
        self.save_path_cost = False
        self.saved_depth = {}
        self.alg.max_depth = self.checked_depth

    def setup_ends_points(self, end_x, end_y):
        super().setup_ends_points(end_x, end_y)
        self.setup_dfs()

    def next_step(self):
        if self.alg.max_depth > self.max_depth:
            self.finish()
            return
        if len(self.alg.stack) == 0:
            self.clean_prev()
            self.saved_depth = self.alg.saved_depth
            self.checked_depth += 1
            self.alg = Dfs()
            self.setup_dfs()
            self.alg.max_depth = self.checked_depth
        self.matrix = self.alg.matrix
        self.alg.next_step()
        self.changes.extend(self.alg.get_changes())
        self.lines.extend(self.alg.get_lines())
        self.should_clean_lines = self.alg.clean_lines()
        self.walker_x = self.alg.walker_x
        self.walker_y = self.alg.walker_y

    def setup_dfs(self):
        self.alg.setup_matrix(self.matrix)
        self.alg.setup_starts_points(self.start_x, self.start_y)
        self.alg.setup_ends_points(self.end_x, self.end_y)
        self.alg.save_path_cost = self.save_path_cost
        self.alg.saved_depth = self.saved_depth

    def clean_prev(self):
        if self.alg.checking is not None:
            item = self.alg.checking
            while item != item.get_parent():
                self.set_status(item.get_x(), item.get_y(), FieldType.CLEAR)
                item = item.get_parent()
            self.alg.checking = None
