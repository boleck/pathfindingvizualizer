import math

from algorithms.algorithm import Algorithm
from selection import FieldType
from utils.connection import Connection
from utils.directions import Direction


class Bfs(Algorithm):
    def __init__(self):
        super().__init__()
        self.fifo = []
        self.visited = []
        self.found = False
        self.path_to_goal = []
        self.checking = None
        self.to_check = []
        self.end_field = FieldType.END
        self.check_field = FieldType.CHECK

    def setup_starts_points(self, start_x, start_y):
        super().setup_starts_points(start_x, start_y)
        start = Connection(None, (start_x, start_y))
        self.checking = start
        self.to_check = self.get_available_children(start)
        self.set_status(self.checking.get_x(), self.checking.get_y(), FieldType.DEADEND)
        self.visited.append((self.checking.get_points()))

    def set_field_to_end(self, field):
        self.end_field = field

    def set_check_field(self, field):
        self.check_field = field

    def next_step(self):
        if not self.found:
            self.add_child_to_check()
            if self.to_check_is_empty():
                self.next_field_to_check()
        else:
            self.show_path_to_end()

    def get_available_children(self, item):
        directions = []
        check_list = Direction.get_direction_diagonal_last()
        for e in check_list:
            point = tuple(map(sum, zip(item.get_points(), e.points())))
            if not self.visited.__contains__(point) and self.get_field(point[0], point[1]) != FieldType.WALL:
                connection = Connection(item, point)
                directions.append(connection)
                self.visited.append(point)
        return directions

    def straight_move(self, from_x, from_y):
        a = (from_x, from_y)
        b = (self.end_x, self.end_y)

        r = math.atan2(b[1] - a[1], b[0] - a[0])
        step = math.pi / 8
        if -step <= r < step:
            return Direction.E.value
        elif step <= r < (math.pi / 2) - step:
            return Direction.SE.value
        elif (math.pi / 2) - step <= r < math.pi / 2 + step:
            return Direction.S.value
        elif math.pi / 2 + step <= r < math.pi - step:
            return Direction.SW.value
        elif math.pi - step <= r or r < -(math.pi - step):
            return Direction.W.value
        elif -(math.pi - step) <= r < -(math.pi / 2 + step):
            return Direction.NW.value
        elif -(math.pi / 2 + step) <= r < -(math.pi / 2 - step):
            return Direction.N.value
        elif -(math.pi / 2 - step) <= r < - step:
            return Direction.NE.value


    def find_item_by_coords(self, x, y):
        for item in self.fifo:
            if item.get_x() == x and item.get_y() == y:
                return item
        return None

    def init_path_to_goal(self, item):
        self.found = True
        while item != item.get_parent():
            self.path_to_goal.append(item)
            item = item.get_parent()

    def add_child_to_check(self):
        if len(self.to_check) != 0:
            item = self.to_check.pop(0)
            self.fifo.append(item)
            y = item.get_y()
            x = item.get_x()
            parent = item.get_parent()
            self.lines.append((parent.get_x(), parent.get_y(), x, y))
            self.check_if_end(x, y, item)
            self.set_status(x, y, self.check_field)

    def next_field_to_check(self):
        self.set_status(self.checking.get_x(), self.checking.get_y(), FieldType.DEADEND)
        self.checking = self.fifo.pop(0)
        self.to_check = self.get_available_children(self.checking)

    def check_if_end(self, x, y, item):
        if self.get_field(x, y) == self.end_field:
            self.init_path_to_goal(item)

    def to_check_is_empty(self):
        return len(self.to_check) == 0

    def show_path_to_end(self):
        self.visited = []
        for _ in range(len(self.path_to_goal)):
            step = self.path_to_goal.pop(0).get_points()
            self.set_status(step[0], step[1], FieldType.PATH)
        self.walker_x = self.end_x
        self.walker_y = self.end_y
