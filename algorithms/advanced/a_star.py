from algorithms.algorithm import Algorithm
from selection import FieldType
from utils.connection import Connection
from utils.directions import Direction
from utils.heuristic import Cluster, Euclidean


class Node(Connection):
    def __init__(self, cost, h, parent, point):
        super().__init__(parent, point, cost)
        self.h = h
        self.f = cost + h


class AStar(Algorithm):
    def __init__(self):
        super().__init__()
        self.open = []
        self.closed = []
        self.path_to_goal = []
        self.current = None
        self.heuristic = None

    def setup_ends_points(self, end_x, end_y):
        super().setup_ends_points(end_x, end_y)
        connection = Node(0, self.goal_dist_estimate((self.start_x, self.start_y)), None, (self.start_x, self.start_y))
        self.open.append(connection)
        self.current = connection

    def next_step(self):
        if len(self.open) == 0:
            self.finish()
            return

        self.current = self.get_smallest_in_open()
        self.set_status(self.current.get_x(), self.current.get_y(), FieldType.DEADEND)

        parent = self.current.parent
        if parent is not None:
            self.lines.append((parent.get_x(), parent.get_y(), self.current.get_x(), self.current.get_y()))
        if self.current.get_x() == self.end_x and self.current.get_y() == self.end_y:
            self.init_path_to_goal(self.current)
            self.show_path_to_end()
            self.finish()
            return

        connections = self.get_available_children(self.current)
        for connection in connections:
            new_cost = self.current.depth + 1
            if self.open.__contains__(connection) or self.closed.__contains__(connection):
                continue
            connection.parent = self.current
            connection.depth = new_cost
            connection.h = self.goal_dist_estimate(connection.point)
            connection.f = connection.depth + connection.h
            if self.closed.__contains__(connection):
                self.closed.remove(connection)
            if not self.open.__contains__(connection):
                self.open.append(connection)
            self.set_status(connection.get_x(), connection.get_y(), FieldType.CHECK)
        self.closed.append(self.current)

    def goal_dist_estimate(self, item):
        return self.heuristic.calculate(item[0], item[1], self.end_x, self.end_y)

    def init_path_to_goal(self, item):
        while item != item.get_parent():
            self.path_to_goal.append(item)
            item = item.get_parent()

    def show_path_to_end(self):
        for _ in range(len(self.path_to_goal)):
            step = self.path_to_goal.pop(0).get_points()
            self.set_status(step[0], step[1], FieldType.PATH)
        self.walker_x = self.end_x
        self.walker_y = self.end_y

    def get_smallest_in_open(self):
        lowest = self.open[0]
        for item in self.open:
            if item.f < lowest.f:
                lowest = item
        self.open.remove(lowest)
        return lowest

    def get_available_children(self, item):
        directions = []
        for e in Direction:
            point = tuple(map(sum, zip(item.get_points(), e.points())))
            if self.get_field(point[0], point[1]) != FieldType.DEADEND \
                    and self.get_field(point[0], point[1]) != FieldType.CHECK \
                    and self.get_field(point[0], point[1]) != FieldType.WALL:
                connection = Node(0, 0, item, point)
                directions.append(connection)
        return directions

    def add_to_open(self, connections):
        connections.extend(self.open)
        self.open = connections


class AStarCluster(AStar):
    def __init__(self):
        super().__init__()
        self.heuristic = Cluster()


class AStarEuclidean(AStar):
    def __init__(self):
        super().__init__()
        self.heuristic = Euclidean()

# from algorithms.algorithm import Algorithm
#
#
# class NodeRecord:
#     def __init__(self, node, connection, cost_so_far, estimated_total_cost):
#         self.node = node
#         self.connection = connection
#         self.cost_so_far = cost_so_far
#         self.estimated_total_cost = estimated_total_cost
#
#
# class Connection2:
#     def __init__(self, cost, from_node, to_node):
#         self.cost = cost
#         self.from_node = from_node
#         self.to_node = to_node
#
#     def get_cost(self):
#         return self.cost
#
#     def get_from_node(self):
#         return self.from_node
#
#     def get_to_node(self):
#         return self.to_node
#
#
# class AStar(Algorithm):
#     def __init__(self):
#         super().__init__()
#         self.open = []
#         self.closed = []
#         self.path_to_goal = []
#         self.current = None
#         self.heuristic = None
#
#     def setup_starts_points(self, start_x, start_y):
#         super().setup_starts_points(start_x, start_y)
#         connection = NodeRecord((start_x, start_y), None, 0, self.heuristic.estimate((start_x, start_y)))
#         self.open.append(connection)
#         self.current = connection
#
#     def next_step(self):
#         if len(self.open) == 0:
#             self.finish()
#             return
#
#         self.current = self.get_smallest_in_open()  # (using the estimatedTotalCost)
#
#         if self.current.get_x() == self.end_x and self.current.get_y() == self.end_y:
#             self.init_path_to_goal(self.current)
#             self.show_path_to_end()
#             self.finish()
#             return
#
#         connections = self.get_available_children(self.current)
#         for connection in connections:
#             end_node = connection.getToNode()
#             end_node_cost = self.current.costSoFar + connection.getCost()
#             if self.closed.__contains__(connection):
#                 end_node_record = self.closed.__getitem__(connection.getNode)
#                 if end_node_record.costSoFar <= end_node_cost:
#                     continue
#                 self.closed.remove(connection)
#                 end_node_heuristic = connection.cost - end_node_record.costSoFar
#             elif self.open.__contains__(connection):
#                 end_node_record = self.open.__getitem__(connection.getNode)
#                 if end_node_record.costSoFar <= end_node_cost:
#                     continue
#                 end_node_heuristic = connection.cost - end_node_record.costSoFar
#             else:
#                 end_node_record = NodeRecord(end_node, )
#                 end_node_record.node = end_node
#                 end_node_heuristic = self.heuristic.estimate(end_node)
#
#             end_node_record.cost = end_node_cost
#             end_node_record.connection = connection
#             end_node_record.estimatedTotalCost = end_node_cost + end_node_heuristic
#             if not self.open.__contains__(end_node):
#                 self.open.append(end_node_record)
#             self.open.remove(self.current)
#             self.closed.append(self.current)
#
#             if self.current.get_x() != self.end_x and self.current.get_y() != self.end_y:
#                 self.finish()
#                 return
#             else:
#                 self.init_path_to_goal(self.current)
#                 self.show_path_to_end()
#                 self.finish()
#
#     def get_smallest_in_open(self):
#         return self.open.pop()  # temp -> zwrócić najmniejszy posortowany estimatedTotalCost
