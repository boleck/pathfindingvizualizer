from algorithms.algorithm import Algorithm
from selection import FieldType
from utils.connection import Connection
from utils.directions import Direction
import math


def cost(points_a, points_b):
    return math.sqrt(math.fabs(points_a[0] - points_b[0]) + math.fabs(points_a[1] - points_b[1]))


class Dijkstra(Algorithm):
    def __init__(self):
        super().__init__()
        self.open = []
        self.path_to_goal = []
        self.current = None

    def setup_starts_points(self, start_x, start_y):
        super().setup_starts_points(start_x, start_y)
        connection = Connection(None, (start_x, start_y), depth=0)
        self.open.append(connection)
        self.current = connection

    def next_step(self):
        if len(self.open) == 0:
            self.finish()
            return
        self.current = self.get_smallest_in_open()
        self.set_status(self.current.get_x(), self.current.get_y(), FieldType.DEADEND)

        parent = self.current.parent
        if parent is not None:
            self.lines.append((parent.get_x(), parent.get_y(), self.current.get_x(), self.current.get_y()))
        if self.current.get_x() == self.end_x and self.current.get_y() == self.end_y:
            self.init_path_to_goal(self.current)
            self.show_path_to_end()
            self.finish()
            return

        connections = self.get_available_children(self.current)
        for connection in connections:
            new_cost = self.current.depth + cost((self.current.get_x(), self.current.get_y()), connection.get_points())
            if self.open.__contains__(connection):
                continue
            connection.parent = self.current
            connection.depth = new_cost
            self.add_to_open([connection])
            self.set_status(connection.get_x(), connection.get_y(), FieldType.CHECK)

    def init_path_to_goal(self, item):
        while item != item.get_parent():
            self.path_to_goal.append(item)
            item = item.get_parent()

    def show_path_to_end(self):
        for _ in range(len(self.path_to_goal)):
            step = self.path_to_goal.pop(0).get_points()
            self.set_status(step[0], step[1], FieldType.PATH)
        self.walker_x = self.end_x
        self.walker_y = self.end_y

    def get_smallest_in_open(self):
        smallest = 0
        for index in range(len(self.open)):
            if self.open[index].depth <= self.open[smallest].depth:
                smallest = index
        return self.open.pop(smallest)

    def get_available_children(self, item):
        directions = []
        for e in Direction:
            point = tuple(map(sum, zip(item.get_points(), e.points())))
            if self.get_field(point[0], point[1]) != FieldType.DEADEND \
                    and self.get_field(point[0], point[1]) != FieldType.CHECK \
                    and self.get_field(point[0], point[1]) != FieldType.WALL:
                connection = Connection(item, point)
                directions.append(connection)
        return directions

    def add_to_open(self, connections):
        connections.extend(self.open)
        self.open = connections





