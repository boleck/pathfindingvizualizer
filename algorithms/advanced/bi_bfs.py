from algorithms.advanced.bfs import Bfs
from algorithms.algorithm import Algorithm
from selection import FieldType


class BiBfs(Algorithm):
    def __init__(self):
        super().__init__()
        self.found = False
        self.switch = True
        self.alg = [Bfs(), Bfs()]
        self.selected = 0
        self.alg[0].set_field_to_end(FieldType.CHECK2)
        self.alg[0].set_check_field(FieldType.CHECK)
        self.alg[1].set_field_to_end(FieldType.CHECK)
        self.alg[1].set_check_field(FieldType.CHECK2)

    def setup_starts_points(self, start_x, start_y):
        super().setup_starts_points(start_x, start_y)
        self.alg[0].setup_starts_points(start_x, start_y)
        self.alg[1].setup_ends_points(start_x, start_y)

    def setup_ends_points(self, end_x, end_y):
        super().setup_ends_points(end_x, end_y)
        self.alg[1].setup_starts_points(end_x, end_y)
        self.alg[0].setup_ends_points(end_x, end_y)

    def setup_matrix(self, matrix):
        self.matrix = matrix
        self.alg[0].setup_matrix(self.matrix)
        self.alg[1].setup_matrix(self.matrix)

    def next_step(self):
        self.current_alg().next_step()
        if self.current_alg().found:
            self.end()
            self.finish()
        self.update()
        self.change_alg()

    def update(self):
        self.changes.extend(self.current_alg().get_changes())
        self.lines.extend(self.current_alg().get_lines())
        self.matrix = self.current_alg().matrix
        self.other_alg().setup_matrix(self.matrix)

    def current_alg(self):
        return self.alg[self.selected]

    def other_alg(self):
        return self.alg[(self.selected - 1) * -1]

    def end(self):
        goal = self.current_alg().path_to_goal[0].get_points()
        item = self.other_alg().find_item_by_coords(goal[0], goal[1])
        self.other_alg().init_path_to_goal(item)
        self.other_alg().show_path_to_end()
        self.current_alg().show_path_to_end()
        self.update()
        self.change_alg()

    def change_alg(self):
        self.selected = (self.selected - 1) * -1


