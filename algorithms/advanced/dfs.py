import math

from algorithms.algorithm import Algorithm
from selection import FieldType
from utils.connection import Connection
from utils.directions import Direction


def descendant_of(point, item):
    while item != item.get_parent():
        if item.get_points() == point:
            return True
        item = item.get_parent()

    return False


class Dfs(Algorithm):
    def __init__(self):
        super().__init__()
        self.stack = []
        self.visited = []
        self.found = False
        self.path_to_goal = []
        self.checking = None
        self.max_depth = 5
        self.save_path_cost = False
        self.saved_depth = {}

    def setup_starts_points(self, start_x, start_y):
        super().setup_starts_points(start_x, start_y)
        start = Connection(None, (start_x, start_y))
        self.checking = start
        self.stack.append(start)
        self.set_status(start_x, start_y, FieldType.DEADEND)
        self.visited.append((start_x, start_y))

    def next_step(self):
        if not self.found:
            self.add_child_to_check()
        else:
            self.show_path_to_end()

    def get_available_children(self, item):
        directions = []
        if item.depth < self.max_depth:
            e = self.straight_move(item.get_x(), item.get_y())
            check_list = Direction.get_neighbour_list(Direction(e))
            for e in check_list:
                point = tuple(map(sum, zip(item.get_points(), e.points())))
                if not self.visited.__contains__(point) and self.get_field(point[0], point[1]) != FieldType.WALL and\
                        not descendant_of(point, item) and\
                        not self.is_crossing_neighbour(point, item) and\
                        (not self.save_path_cost or self.is_depth_smaller(point[0], point[1], item.depth + 1)):
                    connection = Connection(item, point, item.depth + 1)
                    self.saved_depth[(point[0], point[1])] = (item.depth + 1, self.max_depth)
                    directions.append(connection)
        return directions

    def init_path_to_goal(self, item):
        while item != item.get_parent():
            self.path_to_goal.append(item)
            item = item.get_parent()

    def add_child_to_check(self):
        if len(self.stack) == 0:
            self.finish()
            return
        item = self.stack.pop(0)
        prev_x = self.checking.get_x()
        prev_y = self.checking.get_y()
        self.set_status(prev_x, prev_y, FieldType.CLEAR)
        prev = self.checking.get_parent()
        while prev != prev.get_parent():
            self.set_status(prev.get_x(), prev.get_y(), FieldType.CLEAR)
            prev = prev.get_parent()

        self.add_to_stack(self.get_available_children(item))
        y = item.get_y()
        x = item.get_x()

        self.checking = item
        if self.end_x == x and self.end_y == y:
            self.found = True
            self.init_path_to_goal(item)

        while item != item.get_parent():
            self.lines.append((item.get_parent().get_x(), item.get_parent().get_y(), item.get_x(), item.get_y()))
            self.set_status(item.get_x(), item.get_y(), FieldType.DEADEND)
            item = item.get_parent()
        self.should_clean_lines = True
        self.set_status(x, y, FieldType.CHECK)

    def straight_move(self, from_x, from_y):
        a = (from_x, from_y)
        b = (self.end_x, self.end_y)

        r = math.atan2(b[1] - a[1], b[0] - a[0])
        step = math.pi / 8
        if -step <= r < step:
            return Direction.E.value
        elif step <= r < (math.pi / 2) - step:
            return Direction.SE.value
        elif (math.pi / 2) - step <= r < math.pi / 2 + step:
            return Direction.S.value
        elif math.pi / 2 + step <= r < math.pi - step:
            return Direction.SW.value
        elif math.pi - step <= r or r < -(math.pi - step):
            return Direction.W.value
        elif -(math.pi - step) <= r < -(math.pi / 2 + step):
            return Direction.NW.value
        elif -(math.pi / 2 + step) <= r < -(math.pi / 2 - step):
            return Direction.N.value
        elif -(math.pi / 2 - step) <= r < - step:
            return Direction.NE.value

    def show_path_to_end(self):
        self.visited = []
        for _ in range(len(self.path_to_goal)):
            step = self.path_to_goal.pop(0).get_points()
            self.set_status(step[0], step[1], FieldType.PATH)
        self.walker_x = self.end_x
        self.walker_y = self.end_y

    def add_to_stack(self, kids):
        kids.extend(self.stack)
        self.stack = kids

    def is_crossing_neighbour(self, to, from_item):
        _from = from_item.get_points()
        dire = Direction.get_value_by_position(to[0] - _from[0], to[1] - _from[1])
        if dire.is_diagonal():
            a = dire.clockwise_prev_direction().points()
            b = dire.clockwise_next_direction().points()
            dir_a = self.get_connection_by_coords(a[0] + _from[0], a[1] + _from[1], from_item)
            dir_b = self.get_connection_by_coords(b[0] + _from[0], b[1] + _from[1], from_item)
            if dir_a is None or dir_b is None:
                return False
            return dir_a.get_parent() == dir_b or dir_b.get_parent() == dir_a

        return False

    def get_connection_by_coords(self, x, y, connection):
        while connection.get_parent() != connection:
            if connection.get_x() == x and connection.get_y() == y:
                return connection
            connection = connection.get_parent()
        if connection.get_x() == x and connection.get_y() == y:
            return connection
        return None

    def is_depth_smaller(self, x, y, depth):
        if (x, y) in self.saved_depth:
            saved = self.saved_depth[(x, y)]
            if saved is not None:
                return saved[0] > depth or saved[0] == depth and saved[1] < self.max_depth
        return True
