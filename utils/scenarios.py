

def array_to_string(array):
    return str(array).strip('[]')


PATH = "maps/"


class Maps:
    mapka = ["custom", "scenario-1", "scenario-2", "scenario-3", "scenario-4", "scenario-5", "scenario-6", "scenario-7"]

    def save(self, array, nr=0):
        file = open(PATH + Maps.mapka[nr], "w+")
        string = array_to_string(array)
        file.write(string)
        file.close()

    def read(self, nr=0):
        file = open(PATH + Maps.mapka[nr], "r")
        text = file.read()
        file.close()
        return text
