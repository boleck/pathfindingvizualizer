import enum


class Direction(enum.Enum):
    N = 0
    NE = 1
    E = 2
    SE = 3
    S = 4
    SW = 5
    W = 6
    NW = 7

    def points(self):
        return {
            0: (0, -1),
            1: (1, -1),
            2: (1, 0),
            3: (1, 1),
            4: (0, 1),
            5: (-1, 1),
            6: (-1, 0),
            7: (-1, -1)
        }[self.value]

    @staticmethod
    def get_value_by_position(pos_x, pos_y):
        return {
            (0, -1): Direction.N,
            (1, -1): Direction.NE,
            (1, 0): Direction.E,
            (1, 1): Direction.SE,
            (0, 1): Direction.S,
            (-1, 1): Direction.SW,
            (-1, 0): Direction.W,
            (-1, -1): Direction.NW
        }[(pos_x, pos_y)]

    @staticmethod
    def get_neighbour_list(direction):
        a = direction.clockwise_prev_direction()
        b = direction.clockwise_next_direction()
        step_list = [direction, a, b]
        for _ in range(2):
            a = a.clockwise_prev_direction()
            b = b.clockwise_next_direction()
            step_list.append(a)
            step_list.append(b)
        step_list.append(a.clockwise_prev_direction())
        return step_list

    @staticmethod
    def get_direction_diagonal_last():
        step_list = [Direction.N,Direction.E,Direction.S,Direction.W,Direction.NE,Direction.SE,Direction.SW,Direction.NW]
        return step_list

    def is_diagonal(self):
        return {
            0: False,
            1: True,
            2: False,
            3: True,
            4: False,
            5: True,
            6: False,
            7: True
        }[self.value]

    def clockwise_prev_direction(self):
        return Direction((self.value - 1) % 8)

    def clockwise_next_direction(self):
        return Direction((self.value + 1) % 8)
