
class Constant:
    CELL_SIZE = 20
    CANVAS_WIDTH = 1000
    CANVAS_HEIGHT = 600
    CONTROLS_HEIGHT = 90
    OFFSET = 2

    counter_width = CANVAS_WIDTH // CELL_SIZE
    counter_height = CANVAS_HEIGHT // CELL_SIZE
