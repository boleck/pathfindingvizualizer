from utils.utils import Utils


class Color:

    def __init__(self, start_x, start_y, end_x, end_y, start_color, end_color, n=50):
        self.max_destination = Utils.calculate_destination(start_x, start_y, end_x, end_y) * 1.1
        self.colors = get_hex_spectrum(start_color, end_color, n)
        self.n = n
        self.step = self.max_destination / self.n

    def color_to_destination(self, x1, y1, x2, y2):
        return Utils.calculate_destination(x1, y1, x2, y2)

    def get_color(self, x1, y1, x2, y2):
        dest = self.color_to_destination(x1, y1, x2, y2)
        index = (self.n - 1) if int(dest // self.step) >= self.n else int(dest // self.step)
        return self.colors[index]


def hex_to_RGB(hex):
    ''' "#FFFFFF" -> [255,255,255] '''
    # Pass 16 to the integer function for change of base
    return [int(hex[i:i + 2], 16) for i in range(1, 6, 2)]


def RGB_to_hex(RGB):
    ''' [255,255,255] -> "#FFFFFF" '''
    # Components need to be integers for hex to make sense
    RGB = [int(x) for x in RGB]
    return "#" + "".join(["0{0:x}".format(v) if v < 16 else
                          "{0:x}".format(v) for v in RGB])


def get_hex_spectrum(start, end, n=20):
    start_rgb = hex_to_RGB(start)
    end_rgb = hex_to_RGB(end)

    colors = []

    for i in range(n):
        a = [0, 0, 0]
        a[0] = int(start_rgb[0] + ((end_rgb[0] - start_rgb[0]) / (n - 1)) * i)
        a[1] = int(start_rgb[1] + ((end_rgb[1] - start_rgb[1]) / (n - 1)) * i)
        a[2] = int(start_rgb[2] + ((end_rgb[2] - start_rgb[2]) / (n - 1)) * i)
        colors.append(a)
    return [RGB_to_hex(colors[x]) for x in range(len(colors))]
