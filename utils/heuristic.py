from utils.utils import Utils


class Heuristic:

    def calculate(self, x, y, end_x, end_y):
        pass


class Region(Heuristic):
    def __init__(self, begin, end):
        self.begin = begin
        self.end = end

    def contains(self, x, y):
        return self.begin[0] <= x <= self.end[0] and self.begin[1] <= y <= self.end[1]


class Cluster(Heuristic):

    def __init__(self):
        self.lookup = {}
        self.regions = None
        self.setup_lookup_table()
        self.end_id = None

    def setup_lookup_table(self):
        self.lookup = {0: [-1, 1, 23, 1, 29],
                       1: [1, -1, 35, 1, 12],
                       2: [23, 35, -1, 11, 1],
                       3: [1, 1, 11, -1, 1],
                       4: [29, 12, 1, 1, -1]}

        self.regions = [Region((0, 0), (12, 15)), Region((13, 0), (27, 15)), Region((28, 0), (49, 15)),
                        Region((0, 16), (27, 29)), Region((28, 16), (49, 29))]

    def calculate(self, x, y, end_x, end_y):

        if self.end_id is None:
            self.end_id = self.get_region_id(end_x, end_y)

        _id = self.get_region_id(x, y)
        cost = self.lookup[self.end_id][_id]
        if cost == -1:
            return Utils.calculate_destination(x, y, end_x, end_y)
        return cost

    def get_region_id(self, x, y):
        for i in range(len(self.regions)):
            if self.regions[i].contains(x, y):
                return i
        return -1


class Euclidean(Heuristic):

    def calculate(self, x, y, end_x, end_y):
        return Utils.calculate_destination(x, y, end_x, end_y)
