
class Connection:
    def __init__(self, parent, point, depth=0):
        self.parent = parent
        self.point = point
        self.depth = depth

    def get_parent(self):
        if self.parent is None:
            return self
        return self.parent

    def get_depth(self):
        return self.depth

    def get_x(self):
        return self.point[0]

    def get_y(self):
        return self.point[1]

    def get_points(self):
        return self.point
