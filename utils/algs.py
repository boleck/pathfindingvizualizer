import enum

from algorithms.advanced.a_star import AStar, AStarEuclidean, AStarCluster
from algorithms.advanced.bfs import Bfs
from algorithms.advanced.bi_bfs import BiBfs
from algorithms.advanced.dfs import Dfs
from algorithms.advanced.dijkstra import Dijkstra
from algorithms.advanced.iddfs import Iddfs
from algorithms.locals.random_bounce import RandomBounce
from algorithms.locals.robust_trace import RobustTrace
from algorithms.locals.simple_trace import SimpleTrace


class Alg(enum.Enum):
    RandomBounce = 0
    SimpleTrace = 1
    RobustTrace = 2
    Bfs = 3
    Bidirectional_BFS = 4
    Dfs = 5
    Iddfs = 6
    Dijkstra = 7
    AStarEuclidean = 8
    AStarCluster = 9

    def get_algorithm_name(self):
        return {
            0: "RandomBounce",
            1: "SimpleTrace",
            2: "RobustTrace",
            3: "BFS",
            4: "Bidirectional BFS",
            5: "DFS",
            6: "IDDFS",
            7: "Dijkstra",
            8: "A * Euclidean",
            9: "A * Cluster",
        }[self.value]

    @staticmethod
    def get_algorithm_by_name(name):
        return {
            "RandomBounce": RandomBounce(),
            "SimpleTrace": SimpleTrace(),
            "RobustTrace": RobustTrace(),
            "BFS": Bfs(),
            "Bidirectional BFS": BiBfs(),
            "DFS": Dfs(),
            "IDDFS": Iddfs(),
            "Dijkstra": Dijkstra(),
            "A * Euclidean": AStarEuclidean(),
            "A * Cluster": AStarCluster(),
        }[name]
