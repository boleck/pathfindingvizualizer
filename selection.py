import enum


class SelectionButton:
    def __init__(self, field_type, color):
        self.field_type = field_type
        self.color = color


class FieldType(enum.Enum):
    CLEAR = 0
    START = 1
    END = 2
    WALL = 3
    PATH = 4
    DEADEND = 5
    CHECK = 6
    CHECK2 = 7
    WALKER = 8


class SelectionMode:
    clear = SelectionButton(FieldType.CLEAR, "#fff")
    start = SelectionButton(FieldType.START, "#fff")
    end = SelectionButton(FieldType.END, "#fff")
    wall = SelectionButton(FieldType.WALL, "#242424")
    path = SelectionButton(FieldType.PATH, "#73e1f5")
    deadend = SelectionButton(FieldType.DEADEND, "#ffd787")
    check = SelectionButton(FieldType.CHECK, "#d4b1fa")
    walker = SelectionButton(FieldType.CHECK, "#878bff")

    def __init__(self, point_button=wall):
        self.point_button = point_button

    def set_point_type(self, point_type):
        self.point_button = point_type

    def get_point_button(self):
        return self.point_button

    def get_type(self):
        return self.point_button.field_type

    def set_as_start(self):
        self.point_button = SelectionMode.start

    def set_as_end(self):
        self.point_button = SelectionMode.end

    def set_as_wall(self):
        self.point_button = SelectionMode.wall

    def set_as_clear(self):
        self.point_button = SelectionMode.clear

    def is_end_mode(self):
        return self.point_button == SelectionMode.end

    def is_start_mode(self):
        return self.point_button == SelectionMode.start

    def is_wall_mode(self):
        return self.point_button == SelectionMode.wall

    def is_clear_mode(self):
        return self.point_button == SelectionMode.clear

    def is_drawing(self):
        return self.is_clear_mode() or self.is_wall_mode()


def get_type_by_value(x):
    return {
        0: SelectionMode.clear,
        1: SelectionMode.start,
        2: SelectionMode.end,
        3: SelectionMode.wall,
        4: SelectionMode.path,
        5: SelectionMode.deadend,
        6: SelectionMode.check,
        7: SelectionMode.check,
        8: SelectionMode.walker
    }[x]
