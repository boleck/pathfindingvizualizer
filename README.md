# PathFindingVizualizer

Do uruchomienia wymagany <b>Python 3.7.2</b>


### Uruchamianie :
```
py ./main.py
```


### Instrukcja:
 - W celu zmienić pozycje początku i konica należy przeciągnać myszką odpowiednio zielony i czerwony punkt 
 - Algorytm wyszukiwania ścieżki może zostać wybrany z dropdowna w menu
 - Uruchomienie algorytmu odbywa się przez kliknięcie przycisku start a zatrzymanie przez kliknięcie stop
 - Rysowanie ścian (wybrane domyślnie) odbywa się przez przytrzymanie ppm i poruszanie kursorem po obszarze. Można wybrać opcje tworzenie ścian przez wybranie czarnego przycisku w menu.
 - Usuwanie narysowanych ścian odbywa się przez wybranie szarego przycisku z menu i odbywa się tak samo jak rysowanie ścian
 - Szybkość wykonywania algorytmu odbywa się przez przesunięcie suwakiem w menu.