from board import BoardView, button
from tkinter import *

from utils.algs import Alg
from utils.constants import Constant
from utils.scenarios import Maps

master = Tk()
master.title("PathFinding Visualizer")

OPTIONS = [item.get_algorithm_name() for item in Alg]
SCENARIOS = Maps.mapka

algorithm = StringVar(master)
algorithm.set(OPTIONS[0])

curr_algorithm = StringVar(master)

scenario = StringVar(master)
scenario.set(SCENARIOS[0])

save_cost_value = BooleanVar(master)
save_cost_value.set(False)

hide_grid_value = BooleanVar(master)
hide_grid_value.set(False)

hide_grid_saved = BooleanVar(master)
hide_grid_saved.set(False)

is_drawing_value = BooleanVar(master)
is_drawing_value.set(True)

scale_value = IntVar(master)
scale_value.set(1)

curr_scenario = StringVar(master)
curr_scenario.set(scenario.get())

canvas = Canvas(master, width=Constant.CANVAS_WIDTH + 1, height=Constant.CANVAS_HEIGHT + 1)

board = BoardView(master, 10, 15, 40, 15, canvas, Alg.get_algorithm_by_name(algorithm.get()))


def run():
    if not board.algorithm.is_running or curr_algorithm.get() != algorithm.get():
        curr_algorithm.set(algorithm.get())

        board.set_alg(Alg.get_algorithm_by_name(algorithm.get()))
        board.run()

    if board.paused:
        board.run()


def stop():
    board.stop()

def mouse_move(event):
    cell_x = (event.x - Constant.OFFSET) // Constant.CELL_SIZE
    cell_y = (event.y - Constant.OFFSET) // Constant.CELL_SIZE
    coords_label.config(text="(" + str(cell_x) + ", " + str(cell_y) + ")")


master.minsize(Constant.CANVAS_WIDTH, Constant.CANVAS_HEIGHT + Constant.CONTROLS_HEIGHT)

top_frame = Frame(master, height=100)
top_frame.pack(side=TOP, fill=X)

running_controls = Frame(top_frame, highlightbackground="gray", highlightthickness=1)
running_controls.pack(side=LEFT, fill=BOTH, expand=True)

b_run = Button(running_controls, text="Run", command=run, bg="green", font='Consolas 15 bold')
b_run.pack(side=TOP, fill=BOTH, pady=1)

b_stop = Button(running_controls, text="Stop", command=stop, bg="red", font='Consolas 15 bold')
b_stop.pack(side=TOP, fill=BOTH)

secondary_controls = Frame(top_frame, highlightbackground="gray", highlightthickness=1)
secondary_controls.pack(side=LEFT, fill=BOTH, expand=True)

drawing_controls = Frame(secondary_controls)
drawing_controls.pack(side=TOP)

drawing_label = Label(drawing_controls, text="Map Editor:", font='Consolas 10 underline')
drawing_label.pack(side=LEFT, padx=10)

b_wall = Button(drawing_controls, text="DRAW", command=board.set_as_wall, font='Consolas 12 bold underline')
b_wall.pack(side=LEFT)

b_clear = Button(drawing_controls, text="ERESE", command=board.set_as_clear, font='Consolas 12 bold')
b_clear.pack(side=LEFT)

b_clear_all = Button(drawing_controls, text="CLEAR ALL", command=board.clear_stop, font='Consolas 12 bold')
b_clear_all.pack(side=LEFT)

speed_control = Frame(secondary_controls)
speed_control.pack(side=TOP)

scale_label = Label(speed_control, text="Speed:", font='Consolas 10 underline')
scale_label.grid(row=0, column=0, pady=5, padx=5, sticky="s")
scale = Scale(speed_control, from_=1, to=20, orient=HORIZONTAL, variable=scale_value)
scale.grid(row=0, column=1, pady=3, padx=1, sticky="s")
coords_label = Label(speed_control, text="(0 ,0)", font='Consolas 10')
coords_label.config(width=15)
coords_label.grid(row=0, column=3, pady=3, padx=1, sticky="es")

selectors_controls = Frame(top_frame, highlightbackground="gray", highlightthickness=1)
selectors_controls.pack(side=LEFT, fill=Y)

alg_label = Label(selectors_controls, text="Selected algorithm:", font='Consolas 10 underline')
alg_label.grid(row=0, column=0, pady=5, padx=1, sticky="e")

selector_alg = OptionMenu(selectors_controls, algorithm, *OPTIONS)
selector_alg.config(width=15)
selector_alg.config(font='Consolas 10')
selector_alg.grid(row=0, column=1, pady=3, padx=1)

scenarios_label = Label(selectors_controls, text="Selected scenario:", font='Consolas 10 underline')
scenarios_label.grid(row=1, column=0, pady=5, padx=1, sticky="e")

selector_scenarios = OptionMenu(selectors_controls, scenario, *SCENARIOS)
selector_scenarios.config(width=15)
selector_scenarios.config(font='Consolas 10')
selector_scenarios.grid(row=1, column=1, pady=3, padx=1)

options_controls = Frame(top_frame, highlightbackground="gray", highlightthickness=1)
options_controls.pack(side=LEFT, fill=BOTH, expand=True)

max_depth_label = Label(options_controls, text="Max depth:", font='Consolas 10 underline')
max_depth_label.grid(row=0, column=0, pady=5, padx=4, sticky="e")

max_depth = Spinbox(options_controls, from_=1, to=100)
max_depth.grid(row=0, column=1, pady=5, padx=4)

save_cost_label = Label(options_controls, text="Save path cost:", font='Consolas 10 underline')
save_cost_label.grid(row=1, column=0, pady=5, padx=4, sticky="e")

save_path_cost_checkbox = Checkbutton(options_controls, variable=save_cost_value, font="Consolas 10 underline")
save_path_cost_checkbox.grid(row=1, column=1, pady=5, padx=4, sticky="w")

hide_grid_label = Label(options_controls, text="Hide grid:", font='Consolas 10 underline')
hide_grid_label.grid(row=1, column=1, pady=5, padx=4, sticky="e")

hide_grid = Checkbutton(options_controls, variable=hide_grid_value, font="Consolas 10")
hide_grid.grid(row=1, column=2, pady=5, padx=4, sticky="w")


canvas.bind("<B1-Motion>", board.is_moving)
canvas.bind("<B3-Motion>", board.is_moving)
canvas.bind("<Button-1>", board.callback)
canvas.bind("<Motion>", mouse_move)

canvas.bind("<ButtonRelease-1>", board.button_raised)

canvas.pack(side=BOTTOM)
board.update_changes()

master.after(2000, board.update_changes)



def update_gui():
    if curr_scenario.get() != scenario.get():
        pos = Maps.mapka.index(scenario.get())
        if Maps.mapka.index(curr_scenario.get()) == 0:
            board.save_map()
        board.stop()
        board.read_map(pos)
        curr_scenario.set(scenario.get())
    if scale_value.get() != board.speed.get():
        board.speed.set(scale_value.get())
        board.speed_change()
    if max_depth.get() != board.max_depth_value.get():
        board.max_depth_value.set(max_depth.get())
    if save_cost_value.get() != board.save_cost_value.get():
        board.save_cost_value.set(save_cost_value.get())
    if hide_grid_saved.get() != hide_grid_value.get():
        hide_grid_saved.set(hide_grid_value.get())
        board.update_grid(hide_grid_saved.get())

    if button.is_wall_mode() != is_drawing_value.get():
        if button.is_wall_mode():
            b_wall.configure(font="Consolas 12 bold underline")
            b_clear.configure(font="Consolas 12 bold")
            canvas.configure(cursor="arrow")
            is_drawing_value.set(True)
        elif button.is_clear_mode():
            b_wall.configure(font="Consolas 12 bold")
            b_clear.configure(font="Consolas 12 bold underline")
            canvas.configure(cursor="spraycan")
            is_drawing_value.set(False)

    master.after(100, update_gui)


master.after(100, update_gui)

master.mainloop()
