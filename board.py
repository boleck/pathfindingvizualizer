from tkinter import *
from selection import *
from utils.colors import Color
from utils.constants import Constant
from utils.heuristic import Heuristic, Euclidean, Cluster
from utils.scenarios import Maps

cell_size = Constant.CELL_SIZE
OFFSET = Constant.OFFSET

counter_width = Constant.counter_width
counter_height = Constant.counter_height


class BoardView:

    def __init__(self, master, start_x, start_y, end_x, end_y, canvas, algorithm):
        self.master = master
        self.canvas = canvas
        self.algorithm = algorithm
        self.matrix = [
            [View([self.draw_cell(x, y, SelectionMode.clear.color)], FieldType.CLEAR) for x in range(counter_width)]
            for y in range(counter_height)]
        self.grid = [[None] * (counter_width + 1), [None] * (counter_height + 1)]
        self.paths = []
        self.new_path_to_draw = []
        self.cells_to_change_stack = []

        end_view = View(
            [self.draw_circle(end_x, end_y, "#c40a38"), self.draw_circle(end_x, end_y, "#c40a38", "white", True)],
            FieldType.END)
        start_view = View([self.draw_circle(start_x, start_y, "#1dd13e")], FieldType.START)
        walker = View([self.draw_circle(-1, -1, "white", SelectionMode.walker.color, True)], FieldType.WALKER)

        self.start = Point(start_view, start_x, start_y)
        self.end = Point(end_view, end_x, end_y)
        self.walker = Point(walker, -1, -1)
        self.matrix[start_y][start_x].select_type = FieldType.START
        self.matrix[end_y][end_x].select_type = FieldType.END
        # self.draw_grid()
        self.draw_grid()
        self.drawing_status = SelectionMode.wall
        self.color = None

        self.speed = IntVar(master)
        self.speed.set(1)
        self.max_depth_value = IntVar(master)
        self.max_depth_value.set(1)
        self.save_cost_value = BooleanVar(master)
        self.save_cost_value.set(False)

        self.count_step = 0
        self.step_call = None
        self.paused = False

    def set_status(self, x, y, field_type):
        if self.get_field(x, y) != FieldType.WALL \
                or field_type == FieldType.CLEAR:
            if field_type.value == FieldType.START.value:
                self.change_view_coords(self.start.view.tags, x, y)
                self.matrix[self.start.y][self.start.x].select_type = FieldType.CLEAR
                self.cells_to_change_stack.append((self.start.x, self.start.y))
                self.start.x = x
                self.start.y = y
                self.matrix[y][x].select_type = field_type
                self.cells_to_change_stack.append((x, y))

            elif field_type.value == FieldType.END.value:
                self.change_view_coords(self.end.view.tags, x, y)
                self.matrix[self.end.y][self.end.x].select_type = FieldType.CLEAR
                self.cells_to_change_stack.append((self.end.x, self.end.y))
                self.end.x = x
                self.end.y = y
                self.matrix[y][x].select_type = field_type
                self.cells_to_change_stack.append((x, y))
            elif not self.is_start_or_end(x, y):
                self.matrix[y][x].select_type = field_type
                self.cells_to_change_stack.append((x, y))

    def set_alg(self, algorithm):
        self.paused = False
        self.algorithm = algorithm

    def is_finish(self):
        return self.walker.x == self.end.x and self.walker.y == self.end.y

    def callback(self, event):
        cell_x = (event.x - OFFSET) // cell_size
        cell_y = (event.y - OFFSET) // cell_size
        self.drawing_status = button.get_point_button()
        if self.start.x == cell_x and self.start.y == cell_y:
            self.set_as_start()
        elif self.end.x == cell_x and self.end.y == cell_y:
            self.set_as_end()
        self.set_status(cell_x, cell_y, button.get_type())
        self.update_changes()

    def is_moving(self, event):
        cell_x = (event.x - OFFSET) // cell_size
        cell_y = (event.y - OFFSET) // cell_size
        self.set_status(cell_x, cell_y, button.get_type())
        self.update_changes()

    def update_changes(self):
        for _ in range(len(self.cells_to_change_stack)):
            pos = self.cells_to_change_stack.pop()
            x, y = pos[0], pos[1]
            cell = self.matrix[y][x]
            color = self.color.get_color(x, y, self.end.x, self.end.y) if cell.select_type == FieldType.DEADEND \
                else get_type_by_value(cell.select_type.value).color
            outline_color = color
            if cell.select_type == FieldType.DEADEND:
                outline_color = "#ffffff"
            for tag in cell.tags:
                item = self.canvas.find_withtag(tag)
                self.canvas.itemconfig(item, fill=color, outline=outline_color)
        for _ in range(len(self.new_path_to_draw)):
            item = self.new_path_to_draw.pop()
            line = self.draw_line(item[0] + 0.5, item[1] + 0.5,
                            item[2] + 0.5, item[3] + 0.5, "blue")
            self.paths.append(line)

    def draw_cell(self, x, y, color):
        return self.canvas.create_rectangle((x * cell_size) + OFFSET + 2, (y * cell_size) + OFFSET + 2,
                                            ((x + 1) * cell_size) + OFFSET - 2,
                                            ((y + 1) * cell_size) + OFFSET - 2, outline=color, fill=color, width=4)

    def draw_circle(self, x, y, color, outline=None, is_small=False):
        outline = color if outline is None else outline
        division = 1 if is_small else 0
        move = (cell_size / 5) * division
        width = 3 * division
        return self.canvas.create_oval((x * cell_size) + OFFSET + move, (y * cell_size) + OFFSET + move,
                                       ((x + 1) * cell_size) + OFFSET - move,
                                       ((y + 1) * cell_size) + OFFSET - move, outline=outline, width=width, fill=color)

    def draw_line(self, x1, y1, x2, y2, color):
        return self.canvas.create_line((cell_size * x1) + OFFSET, (cell_size * y1) + OFFSET, (cell_size * x2) + OFFSET,
                                       (cell_size * y2) + OFFSET,
                                       fill=color)

    def draw_grid(self):
        for x in range(counter_width + 1):
            self.grid[0][x] = self.canvas.create_line((cell_size * x) + 2, 0, (cell_size * x) + 2,
                                                      (counter_height * cell_size) + 2, fill="#adadad")
        for y in range(counter_height + 1):
            self.grid[1][y] = self.canvas.create_line(0, (cell_size * y) + 2, (counter_width * cell_size) + 2,
                                                      (cell_size * y) + 2, fill="#adadad")

    def draw_lines(self):
        for x in range(counter_width + 1):
            for y in range(counter_height + 1):
                self.draw_line(item[0] + 0.5, item[1] + 0.5,
                               item[2] + 0.5, item[3] + 0.5, "blue")

    def update_grid(self, hide=False):
        state = "hidden" if hide else "normal"
        for x in range(counter_width + 1):
            tag = self.grid[0][x]
            item = self.canvas.find_withtag(tag)
            self.canvas.itemconfig(item, state=state)

        for y in range(counter_height + 1):
            tag = self.grid[1][y]
            item = self.canvas.find_withtag(tag)
            self.canvas.itemconfig(item, state=state)

    def get_field(self, x, y):
        if x < 0 or y < 0 or y >= len(self.matrix) or x >= len(self.matrix[y]):
            return FieldType.WALL
        return self.matrix[y][x].select_type

    def run(self):
        if self.paused:
            self.paused = False
            self.step()
            return
        self.clear(soft=True)
        self.color = Color(self.start.x, self.start.y, self.end.x, self.end.y, "#00ff00", "#ff0000")
        self.algorithm.set_options(self.get_options())
        self.algorithm.setup_matrix(self.matrix)
        self.algorithm.setup_starts_points(self.start.x, self.start.y)
        self.algorithm.setup_ends_points(self.end.x, self.end.y)
        self.walker.x = self.start.x
        self.walker.y = self.start.y
        self.algorithm.is_running = True
        self.count_step = 0
        self.step()

    def stop(self):
        if self.paused or not self.algorithm.is_running:
            if self.step_call is not None:
                self.master.after_cancel(self.step_call)
            self.algorithm.is_running = False
            self.clear(soft=True)
            self.reset()
            self.paused = False
        else:
            self.master.after_cancel(self.step_call)
            self.paused = True

    def reset(self):
        self.change_view_coords(self.walker.view.tags, -1, -1)
        self.walker.x = -1
        self.walker.y = -1

    def step(self):
        if self.is_finish() == FALSE and self.algorithm.is_running:
            self.matrix = self.algorithm.update_matrix()
            self.algorithm.next_step()
            self.cells_to_change_stack.extend(self.algorithm.get_changes())
            self.change_view_coords(self.walker.view.tags, self.algorithm.walker_x, self.algorithm.walker_y)
            self.new_path_to_draw.extend(self.algorithm.get_lines())
            self.walker.x = self.algorithm.walker_x
            self.walker.y = self.algorithm.walker_y
            if self.algorithm.clean_lines():
                self.algorithm.should_clean_lines = False
                for _ in range(len(self.paths)):
                    item = self.paths.pop()
                    self.canvas.delete(item)
            self.update_changes()
            self.count_step = self.count_step + 1
            if not self.paused:
                self.step_call = self.master.after((20 - self.speed.get()) * 5, self.step)
        else:
            count_cover = self.count_cover()
            print("Liczba kroków do odnalezienia celu " + str(self.count_step))
            print("Pokryty obszar: " + str(count_cover[0]) + "/" + str(count_cover[1] + count_cover[0]) + " = " + str((count_cover[0]/(count_cover[1] + count_cover[0]))*100))
            self.reset()
            self.algorithm.is_running = False

    def count_cover(self):
        empty = 0
        visited = 0
        for x in range(counter_width):
            for y in range(counter_height):
                _type = self.get_field(x, y)
                if _type == FieldType.DEADEND or _type == FieldType.CHECK or _type == FieldType.CHECK2 or _type == FieldType.PATH:
                    visited = visited + 1
                elif _type == FieldType.CLEAR:
                    empty = empty + 1
        return visited, empty

    def change_view_coords(self, tags, x, y):
        for tag in tags:
            coords = self.canvas.coords(tag)
            x1 = coords[0] // cell_size
            y1 = coords[1] // cell_size

            ax = (coords[0] - (x1 * cell_size) - OFFSET) + (x * cell_size) + OFFSET
            ay = (coords[1] - (y1 * cell_size) - OFFSET) + (y * cell_size) + OFFSET

            bx = ((x1 + 1) * cell_size) + OFFSET - coords[0] + (x * cell_size) + OFFSET
            by = ((y1 + 1) * cell_size) + OFFSET - coords[1] + (y * cell_size) + OFFSET

            self.canvas.coords(tag, ax, ay, bx, by)

    def speed_change(self):
        if self.step_call is not None:
            self.master.after_cancel(self.step_call)
            self.step()

    def clear(self, soft=False):
        for x in range(counter_width):
            for y in range(counter_height):
                item = self.get_field(x, y)
                if not (item == FieldType.START or item == FieldType.END or item == FieldType.WALL and soft):
                    self.set_status(x, y, FieldType.CLEAR)
                    self.cells_to_change_stack.append((x, y))
        for _ in range(len(self.paths)):
            item = self.paths.pop()
            self.canvas.delete(item)
        self.set_status(self.start.x, self.start.y, FieldType.START)
        self.set_status(self.end.x, self.end.y, FieldType.END)
        self.update_changes()

    def clear_stop(self):
        self.paused = True
        self.stop()
        self.clear()

    def save_map(self):
        _map = Maps()
        array = self.get_data_from_matrix()
        _map.save(array)

    def read_map(self, pos):
        if self.algorithm.is_running:
            self.stop()
        _map = Maps()
        data = _map.read(pos)
        self.update_matrix_from_data(data)

    def button_raised(self, event):
        button.set_point_type(self.drawing_status)

    def set_as_end(self):
        button.set_as_end()

    def set_as_start(self):
        button.set_as_start()

    def set_as_wall(self):
        button.set_as_wall()

    def set_as_clear(self):
        button.set_as_clear()

    def is_start_or_end(self, x, y):
        return (self.end.x == x and self.end.y == y) or (self.start.x == x and self.start.y == y)

    def get_data_from_matrix(self):
        def row_map(row):
            return map(item_map, row)

        def item_map(view):
            return view.select_type.value

        return [item for list in map(row_map, self.matrix) for item in list]

    def update_matrix_from_data(self, data):
        arr = data.split(",")
        cells = len(self.matrix[0])
        y = -1
        for item in range(len(arr)):
            x = item % cells
            if x == 0:
                y = y + 1
            field = get_type_by_value(int(arr[item])).field_type
            self.update_field(x, y, field)
        self.update_changes()

    def get_fields_to_clear(self, soft):
        if soft:
            return

    def get_options(self):
        return {"max_depth": int(self.max_depth_value.get()),
                "save_path_cost": self.save_cost_value.get()}

    def update_field(self, x, y, field):
        if field.value == FieldType.START.value:
            self.change_view_coords(self.start.view.tags, x, y)
            self.start.x = x
            self.start.y = y

        elif field.value == FieldType.END.value:
            self.change_view_coords(self.end.view.tags, x, y)
            self.end.x = x
            self.end.y = y

        self.matrix[y][x].select_type = field
        self.cells_to_change_stack.append((x, y))


class View:
    def __init__(self, tags, select_type):
        self.tags = tags
        self.select_type = select_type


class Point:
    def __init__(self, view, x, y):
        self.view = view
        self.x = x
        self.y = y


button = SelectionMode()
